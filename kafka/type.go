package kafka

func ToInt32(in *int32) int32 {
	return *in
}

func ToPInt32(in int32) *int32 {
	return &in
}

type IClient interface {
	InitConsumerGroup(consumerGroup string, brokerURLs ...string) error
	// InitConsumer depredicated
	InitConsumer(brokerURLs ...string) error
	InitPublisher(brokerURLs ...string) // backward compatible
	// Publish send multiple messages to topic
	Publish(topic string, messages ...interface{}) error
	// OnScanMessages depredicated
	OnScanMessages(topics []string, bufMessage chan Message) error
	// ListTopics for ping
	ListTopics(brokers ...string) ([]string, error)
	// OnAsyncSubscribe subscribe message from list topics,
	// numberPuller is number worker goroutines for pull message from kafka server to message chan
	OnAsyncSubscribe(topics []*Topic, numberPuller int, buf chan Message) error
	// PublishWithConfig help we can publish message to 1 partition.
	// help application process task synchronized
	PublishWithConfig(topic *Topic, config *SenderConfig, messages ...interface{}) error
	Close() error
}
