package restaurantmodel

import (
	"errors"
)

var (
	ErrNameCannotBlank    = errors.New("name cannot blank")
	ErrAddressCannotBlank = errors.New("address cannot blank")
)
