package ginrestaurant

import (
	"g06-restaurantlike-service/common"
	"g06-restaurantlike-service/component/appctx"
	bizrestaurant "g06-restaurantlike-service/modules/restaurant/business"
	restaurantmodel "g06-restaurantlike-service/modules/restaurant/model"
	restaurantstore "g06-restaurantlike-service/modules/restaurant/storage"
	restaurantlikestorage "g06-restaurantlike-service/modules/restaurantlike/storage"
	"net/http"

	"github.com/gin-gonic/gin"
)

func ListRestaurant(appContext appctx.AppContext) func(ctx *gin.Context) {
	return func(c *gin.Context) {
		var filter restaurantmodel.Filter
		var paging common.Paging
		paging.Process()
		if err := c.ShouldBind(&filter); err != nil {
			panic(common.ErrInternal(err))
		}

		// if err := db.Table(Restaurant{}.TableName()).Count(&paging.Total).Error; err != nil {
		// 	c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		// 	return
		// }

		store := restaurantstore.NewSQLStore(appContext.GetMainDBConnection())
		storeResLike := restaurantlikestorage.NewSQLStore(appContext.GetMainDBConnection())
		biz := bizrestaurant.NewListRestaurantBiz(store, storeResLike)

		result, err := biz.ListRestaurant(c.Request.Context(), &filter, &paging)
		if err != nil {
			panic(err)
		}

		for i := range result {
			result[i].Mask(false)
		}
		c.JSON(http.StatusOK, common.NewSuccessResponse(result, paging, filter))
	}
}
