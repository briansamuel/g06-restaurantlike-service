package bizrestaurant

import (
	"context"
	"g06-restaurantlike-service/common"
	restaurantmodel "g06-restaurantlike-service/modules/restaurant/model"
)

type UpdateStore interface {
	GetDataWithCondition(
		ctx context.Context,
		cond map[string]interface{}) (*restaurantmodel.Restaurant, error)
	Update(ctx context.Context, id int, data *restaurantmodel.RestaurantUpdate) error
}

type updateRestaurantBiz struct {
	store DeleteStore
}

func NewUpdateRestaurantBiz(store DeleteStore) *deleteRestaurantBiz {
	return &deleteRestaurantBiz{store: store}
}

func (biz *deleteRestaurantBiz) UpdateRestaurant(ctx context.Context, id int, data *restaurantmodel.RestaurantUpdate) error {

	if err := data.Validate(); err != nil {
		return common.ErrFieldCannotBlank(restaurantmodel.EntityName, err)
	}
	oldData, err := biz.store.GetDataWithCondition(ctx, map[string]interface{}{"id": id})

	if err != nil {
		return common.ErrCannotUpdateEntity(restaurantmodel.EntityName, err)
	}
	if oldData.Status == 0 {
		return common.ErrEntityNotFound(restaurantmodel.EntityName, err)
	}

	if err := biz.store.Update(ctx, id, data); err != nil {

		return common.ErrCannotUpdateEntity(restaurantmodel.EntityName, err)
	}
	return nil
}
