package bizrestaurant

import (
	"context"
	"g06-restaurantlike-service/common"
	restaurantmodel "g06-restaurantlike-service/modules/restaurant/model"
)

type CreateStore interface {
	Create(ctx context.Context, data *restaurantmodel.RestaurantCreate) error
}

type createRestaurantBiz struct {
	store CreateStore
}

func NewCreateRestaurantBiz(store CreateStore) *createRestaurantBiz {
	return &createRestaurantBiz{store: store}
}

func (biz *createRestaurantBiz) CreateRestaurant(ctx context.Context, data *restaurantmodel.RestaurantCreate) error {

	err := biz.store.Create(ctx, data)
	if err != nil {
		return common.ErrCannotCreateEntity(restaurantmodel.EntityName, err)
	}

	return nil

}
