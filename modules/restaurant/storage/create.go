package restaurantstore

import (
	"context"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"os"

	"g06-restaurantlike-service/common"
	restaurantmodel "g06-restaurantlike-service/modules/restaurant/model"
)

func (s *sqlStore) Create(ctx context.Context, data *restaurantmodel.RestaurantCreate) error {
	db := s.db

	tr := otel.Tracer(os.Getenv("SERVICE_NAME"))
	_, span := tr.Start(ctx, "Authentication")

	if err := db.Create(data).Error; err != nil {
		return common.ErrDB(err)
	}
	span.SetAttributes(attribute.Key("restaurant_id").Int(data.ID))
	span.End()

	return nil
}
