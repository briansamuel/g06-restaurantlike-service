package restaurantstore

import (
	"context"
	"g06-restaurantlike-service/common"
	restaurantmodel "g06-restaurantlike-service/modules/restaurant/model"
)

func (s *sqlStore) Delete(ctx context.Context, id int) error {
	db := s.db

	if err := db.Table(restaurantmodel.Restaurant{}.TableName()).Where("id = ?", id).Delete(nil).Error; err != nil {

		return common.ErrDB(err)
	}

	return nil
}
