package restaurantstore

import (
	"context"
	"go.opentelemetry.io/otel"
	"os"

	"g06-restaurantlike-service/common"
	restaurantmodel "g06-restaurantlike-service/modules/restaurant/model"
)

func (s *sqlStore) ListDataWithCondition(ctx context.Context,
	filter *restaurantmodel.Filter,
	paging *common.Paging) ([]restaurantmodel.Restaurant, error) {
	tr := otel.Tracer(os.Getenv("SERVICE_NAME"))
	_, span := tr.Start(ctx, "List Restaurant")
	defer span.End()
	db := s.db
	var result []restaurantmodel.Restaurant

	db = db.Where("status in (?)", 1)

	if filter.OwnerId > 0 {
		db = db.Where("owner_id = ?", filter.OwnerId)
	}
	if err := db.Table(restaurantmodel.Restaurant{}.TableName()).Count(&paging.Total).Error; err != nil {
		return nil, common.ErrDB(err)
	}

	if err := db.
		Limit(paging.Limit).
		Offset((paging.Page - 1) * paging.Limit).
		Order("id desc").
		Find(&result).Error; err != nil {

		return nil, common.ErrDB(err)
	}

	return result, nil
}
