package restaurantlikestore

import (
	"context"
	"g06-restaurantlike-service/common"

	restaurantlikemodel "g06-restaurantlike-service/modules/restaurantlike/model"
	"gorm.io/gorm"
)

func (s *sqlStore) GetDataWithCondition(ctx context.Context,
	cond map[string]interface{}) (*restaurantlikemodel.Like, error) {

	var data restaurantlikemodel.Like
	if err := s.db.WithContext(ctx).
		Where(cond).
		First(&data).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, common.RecordNotFound
		}
		return nil, common.ErrDB(err)
	}

	return &data, nil
}
