package restaurantlikebiz

import (
	"context"
	"g06-restaurantlike-service/common"
	"g06-restaurantlike-service/kafka"
	restaurantlikemodel "g06-restaurantlike-service/modules/restaurantlike/model"
	trace "github.com/briansamuel/traceprovider/otel"
)

type UserDislikeRestaurantStorage interface {
	Delete(ctx context.Context, data *restaurantlikemodel.Like) error
	GetDataWithCondition(ctx context.Context,
		cond map[string]interface{}) (*restaurantlikemodel.Like, error)
}

//type DecreaseLikeStore interface {
//	DecreaseLikeCount(ctx context.Context, id int) error
//}

type userDislikeRestaurantBiz struct {
	store UserDislikeRestaurantStorage
	//storeDecrease DecreaseLikeStore
	kaf kafka.PubSub
}

func NewUserDislikeRestaurantBiz(
	store UserDislikeRestaurantStorage,
//storeDecrease DecreaseLikeStore,
	kaf kafka.PubSub,
) *userDislikeRestaurantBiz {
	return &userDislikeRestaurantBiz{
		store: store,
		//storeDecrease: storeDecrease,
		kaf: kaf,
	}
}

func (biz *userDislikeRestaurantBiz) DislikeRestaurant(ctx context.Context, data *restaurantlikemodel.Like) error {

	tr := trace.Tracer()
	ctx, span := tr.Start(ctx, "Business Dislike Restaurant")
	defer span.End()

	_, err := biz.store.GetDataWithCondition(ctx, map[string]interface{}{"restaurant_id": data.RestaurantID, "user_id": data.UserId})
	if err != nil {
		return restaurantlikemodel.ErrCannotDisLikeRestaurant(err)
	}

	if err := biz.store.Delete(ctx, data); err != nil {
		return restaurantlikemodel.ErrCannotDisLikeRestaurant(err)
	}

	// Use PubSub to increase like
	go func() {
		defer common.Recover()
		//_ = biz.pb.Publish(ctx, common.TopicUserDislikeRestaurant, pubsub.NewMessage(data))
		_ = biz.kaf.Publish(ctx, common.TopicUserDislikeRestaurant, data)
	}()

	return nil
}
