package restaurantlikebiz

import (
	"context"
	"g06-restaurantlike-service/common"
	"g06-restaurantlike-service/kafka"
	restaurantlikemodel "g06-restaurantlike-service/modules/restaurantlike/model"
	trace "github.com/briansamuel/traceprovider/otel"
)

type UserLikeRestaurantStorage interface {
	Create(ctx context.Context, data *restaurantlikemodel.Like) error
	GetDataWithCondition(ctx context.Context,
		cond map[string]interface{}) (*restaurantlikemodel.Like, error)
}

//type IncreaseLikeStore interface {
//	IncreaseLikeCount(ctx context.Context, id int) error
//}

type userLikeRestaurantBiz struct {
	store UserLikeRestaurantStorage
	//storeIncrease IncreaseLikeStore
	kaf kafka.PubSub
}

func NewUserLikeRestaurantBiz(
	store UserLikeRestaurantStorage,
//storeIncrease IncreaseLikeStore,
	kaf kafka.PubSub,
) *userLikeRestaurantBiz {
	return &userLikeRestaurantBiz{
		store: store,
		//storeIncrease: storeIncrease,
		kaf: kaf,
	}
}

func (biz *userLikeRestaurantBiz) LikeRestaurant(ctx context.Context, data *restaurantlikemodel.Like) error {

	tr := trace.Tracer()
	ctx, span := tr.Start(ctx, "Business Like Restaurant")
	defer span.End()

	_, err := biz.store.GetDataWithCondition(ctx, map[string]interface{}{"restaurant_id": data.RestaurantID, "user_id": data.UserId})
	if err != common.RecordNotFound {
		return restaurantlikemodel.ErrCannotLikeRestaurant(err)
	}

	if err := biz.store.Create(ctx, data); err != nil {
		return restaurantlikemodel.ErrCannotLikeRestaurant(err)
	}

	// Use PubSub to increase like
	go func() {
		defer common.Recover()
		//_ = biz.pb.Publish(ctx, common.TopicUserLikeRestaurant, pubsub.NewMessage(data))
		_ = biz.kaf.Publish(ctx, common.TopicUserLikeRestaurant, data)
	}()
	return nil
}
