package restaurantlikegin

import (
	"g06-restaurantlike-service/common"
	"g06-restaurantlike-service/component/appctx"
	userlikerestaurantbiz "g06-restaurantlike-service/modules/restaurantlike/biz"
	restaurantlikemodel "g06-restaurantlike-service/modules/restaurantlike/model"
	restaurantlikestorage "g06-restaurantlike-service/modules/restaurantlike/storage"
	"github.com/gin-gonic/gin"
	"net/http"
)

func UsersLikeRestaurant(appContext appctx.AppContext) func(ctx *gin.Context) {
	return func(c *gin.Context) {
		db := appContext.GetMainDBConnection()
		var paging common.Paging
		paging.Process()

		uid, err := common.FromBase58(c.Param("id"))
		if err != nil {
			panic(common.ErrInvalidRequest(err))
		}

		filter := restaurantlikemodel.Filter{
			RestaurantID: int(uid.GetLocalID()),
		}
		if err := c.ShouldBind(&paging); err != nil {
			panic(common.ErrInternal(err))
		}
		store := restaurantlikestorage.NewSQLStore(db)
		biz := userlikerestaurantbiz.NewUsersLikeRestaurantBiz(store)

		result, err := biz.ListUserLikeRestaurant(c.Request.Context(), &filter, &paging)
		if err != nil {
			panic(err)
		}
		for i := range result {
			result[i].Mask(false)
		}
		c.JSON(http.StatusOK, common.SimpleSuccessResponse(result))
	}
}
