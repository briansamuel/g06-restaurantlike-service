package restaurantlikegin

import (
	"g06-restaurantlike-service/common"
	"g06-restaurantlike-service/component/appctx"
	userlikerestaurantbiz "g06-restaurantlike-service/modules/restaurantlike/biz"
	restaurantlikemodel "g06-restaurantlike-service/modules/restaurantlike/model"
	restaurantlikestorage "g06-restaurantlike-service/modules/restaurantlike/storage"
	"github.com/gin-gonic/gin"
	"net/http"
)

func Dislike(appContext appctx.AppContext) func(ctx *gin.Context) {
	return func(c *gin.Context) {
		db := appContext.GetMainDBConnection()
		uid, err := common.FromBase58(c.Param("id"))
		if err != nil {
			panic(common.ErrInvalidRequest(err))
		}

		requester := c.MustGet(common.CurrentUser).(common.Requester)

		data := restaurantlikemodel.Like{
			RestaurantID: int(uid.GetLocalID()),
			UserId:       requester.GetUserId(),
		}
		store := restaurantlikestorage.NewSQLStore(db)
		//countStore := restaurantstore.NewSQLStore(db)
		kafka := appContext.GetKafka()
		biz := userlikerestaurantbiz.NewUserDislikeRestaurantBiz(store, kafka)

		if err := biz.DislikeRestaurant(c.Request.Context(), &data); err != nil {
			panic(err)
		}

		c.JSON(http.StatusOK, common.SimpleSuccessResponse(true))
	}
}
