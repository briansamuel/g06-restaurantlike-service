package restaurantlikemodel

import (
	"g06-restaurantlike-service/common"
)

func ErrCannotLikeRestaurant(err error) *common.AppError {
	return common.NewCustomError(err,
		"cannot like restaurant",
		"ErrCannotLikeRestaurant")
}

func ErrCannotDisLikeRestaurant(err error) *common.AppError {
	return common.NewCustomError(err,
		"cannot dislike restaurant",
		"ErrCannotDisLikeRestaurant")
}
