package userstorage

import (
	"context"
	"fmt"
	"g06-restaurantlike-service/common"
	"g06-restaurantlike-service/component/httpClient"
	usermodel "g06-restaurantlike-service/modules/user/model"
	log "github.com/sirupsen/logrus"
)

type Response struct {
	Page       int             `json:"page"`
	PerPage    int             `json:"per_page"`
	Total      int             `json:"total"`
	TotalPages int             `json:"total_pages"`
	Data       *usermodel.User `json:"data"`
}

func (s *sqlStore) FindUser(ctx context.Context, conditions map[string]interface{}) (*usermodel.User, error) {
	data := &Response{}
	service := common.ListService["AUTH_SERVICE"]
	token := conditions["token"].(string)
	var url = fmt.Sprintf("%s/v1/users/profile", service)
	client := httpClient.NewHttpClient()

	if err := client.SendGet(url, token, data); err != nil {
		log.Println(err)
		return nil, common.ErrDB(err)
	}

	//log.Println(data.Data)
	return data.Data, nil
}
