package memcache

import (
	"context"
	"fmt"
	usermodel "g06-restaurantlike-service/modules/user/model"

	"sync"
)

type RealStore interface {
	FindUser(ctx context.Context, conditions map[string]interface{}) (*usermodel.User, error)
}

type UserCaching struct {
	store     *Caching
	realStore RealStore
	once      *sync.Once
}

func NewUserCaching(store *Caching, realStore RealStore) *UserCaching {
	return &UserCaching{
		store:     store,
		realStore: realStore,
		once:      new(sync.Once),
	}
}

func (uc *UserCaching) FindUser(ctx context.Context, conditions map[string]interface{}) (*usermodel.User, error) {
	userId := conditions["id"].(int)
	key := fmt.Sprintf("user-%d", userId)

	userInCache := uc.store.Read(key)

	if userInCache != nil {
		return userInCache.(*usermodel.User), nil
	}
	uc.once.Do(func() {
		user, err := uc.realStore.FindUser(ctx, conditions)

		if err != nil {
			panic(err)
		}

		uc.store.Write(key, user)
	})

	return uc.store.Read(key).(*usermodel.User), nil
}
