package common

import "time"

type SQLModel struct {
	ID        int        `json:"-" gorm:"column:id;"`
	FakeId    UID        `json:"id" gorm:"-"`
	CreatedAt *time.Time `json:"createdAt" gorm:"column:created_at;"` // Thêm * vào type để lấy null
	UpdatedAt *time.Time `json:"updatedAt" gorm:"column:updated_at;"` // Thêm * vào type để lấy null
	Status    int        `json:"status" gorm:"column:status;default:1;"`
}

func (sm *SQLModel) GenUID(objType int) {
	sm.FakeId = NewUID(uint32(sm.ID), objType, 1)
}
